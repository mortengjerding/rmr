import argparse
import json
from os.path import exists, splitext, isfile
from os import remove, chdir, makedirs
from glob import glob

import numpy as np
from gpaw import GPAW
from gpaw.mpi import world
from c2db.berryphase import get_polarization_phase

from ase.parallel import paropen
from ase.units import Bohr
from ase.io import jsonio


def get_wavefunctions(atoms, name, params, density=6.0,
                      no_symmetries=False):
    params['kpts'] = {'density': density,
                      'gamma': True,
                      'even': True}
    if no_symmetries:
        params['symmetry'] = {'point_group': False,
                              'do_not_symmetrize_the_density': False,
                              'time_reversal': False}
    else:
        params['symmetry'] = {'point_group': True,
                              'do_not_symmetrize_the_density': False,
                              'time_reversal': True}
    params['convergence']['eigenstates'] = 1e-11
    tmp = splitext(name)[0]
    atoms.calc = GPAW(txt=tmp + '.txt', **params)
    atoms.get_potential_energy()
    atoms.calc.write(name, 'all')
    return atoms.calc


def borncharges(displacement=0.01, kpointdensity=6.0, folder=None,
                no_symmetries=False):

    if folder is None:
        folder = 'data-borncharges'

    if world.rank == 0:
        try:
            makedirs(folder)
        except FileExistsError:
            pass
    world.barrier()
    
    chdir(folder)
    calc = GPAW('../gs.gpw', txt=None)
    params = calc.parameters
    atoms = calc.atoms
    cell_cv = atoms.get_cell() / Bohr
    vol = abs(np.linalg.det(cell_cv))
    sym_a = atoms.get_chemical_symbols()

    pos_av = atoms.get_positions().copy()
    atoms.set_positions(pos_av)
    Z_avv = []
    P_asvv = []
    
    if world.rank == 0:
        print('Atomnum Atom Direction Displacement')
    for a in range(len(atoms)):
        phase_scv = np.zeros((2, 3, 3), float)
        for v in range(3):
            for s, sign in enumerate([-1, 1]):
                if world.rank == 0:
                    print(sym_a[a], a, v, s)
                # Update atomic positions
                atoms.positions = pos_av
                atoms.positions[a, v] = pos_av[a, v] + sign * displacement
                prefix = 'born-{}-{}{}{}'.format(displacement, a,
                                                 'xyz'[v],
                                                 ' +-'[sign])
                name = prefix + '.gpw'
                berryname = prefix + '-berryphases.json'
                if not exists(name) and not exists(berryname):
                    calc = get_wavefunctions(atoms, name, params,
                                             density=kpointdensity,
                                             no_symmetries=no_symmetries)

                try:
                    phase_c = get_polarization_phase(name)
                except ValueError:
                    calc = get_wavefunctions(atoms, name, params,
                                             density=kpointdensity,
                                             no_symmetries=no_symmetries)
                    phase_c = get_polarization_phase(name)

                phase_scv[s, :, v] = phase_c

                if exists(berryname):  # Calculation done?
                    if world.rank == 0:
                        # Remove gpw file
                        if isfile(name):
                            remove(name)

        dphase_cv = (phase_scv[1] - phase_scv[0])
        mod_cv = np.round(dphase_cv / (2 * np.pi)) * 2 * np.pi
        dphase_cv -= mod_cv
        phase_scv[1] -= mod_cv
        dP_vv = (-np.dot(dphase_cv.T, cell_cv).T /
                 (2 * np.pi * vol))

        P_svv = (-np.dot(cell_cv.T, phase_scv).transpose(1, 0, 2) /
                 (2 * np.pi * vol))
        Z_vv = dP_vv * vol / (2 * displacement / Bohr)
        P_asvv.append(P_svv)
        Z_avv.append(Z_vv)

    data = {'Z_avv': Z_avv, 'sym_a': sym_a,
            'P_asvv': P_asvv}

    filename = 'borncharges-{}.json'.format(displacement)

    with paropen(filename, 'w') as fd:
        json.dump(jsonio.encode(data), fd)

    world.barrier()
    if world.rank == 0:
        files = glob('born-*.gpw')
        for f in files:
            if isfile(f):
                remove(f)


def polvsatom(row, *filenames):
    if 'borndata' not in row.data:
        return

    from matplotlib import pyplot as plt
    borndata = row.data['borndata']
    deltas = borndata[0]
    P_davv = borndata[1]

    for a, P_dvv in enumerate(P_davv.transpose(1, 0, 2, 3)):
        fname = 'polvsatom{}.png'.format(a)
        for fname2 in filenames:
            if fname in fname2:
                break
        else:
            continue

        Pm_vv = np.mean(P_dvv, axis=0)
        P_dvv -= Pm_vv
        plt.plot(deltas, P_dvv[:, 0, 0], '-o', label='xx')
        plt.plot(deltas, P_dvv[:, 1, 1], '-o', label='yy')
        plt.plot(deltas, P_dvv[:, 2, 2], '-o', label='zz')
        plt.xlabel('Displacement (Å)')
        plt.ylabel('Pol')
        plt.legend()
        plt.tight_layout()
        plt.savefig(fname2)
        plt.close()


def webpanel(row):
    from mcr.custom import fig
    polfilenames = []
    if 'Z_avv' in row.data:
        def matrixtable(M, digits=2):
            table = M.tolist()
            shape = M.shape
            for i in range(shape[0]):
                for j in range(shape[1]):
                    value = table[i][j]
                    table[i][j] = '{:.{}f}'.format(value, digits)
            return table

        panel = [[], []]
        for a, Z_vv in enumerate(row.data.Z_avv):
            Zdata = matrixtable(Z_vv)

            Ztable = dict(
                header=[str(a), row.symbols[a], ''],
                type='table',
                rows=Zdata)

            panel[0].append(Ztable)
            polname = 'polvsatom{}.png'.format(a)
            panel[1].append(fig(polname))
            polfilenames.append(polname)
    panel = [('Born charges', panel)]
    return panel, polvsatom, polfilenames


def collect_data(kvp, data, atoms, verbose=False):
    import os.path as op
    delta = 0.01

    P_davv = []

    fname = 'data-borncharges/borncharges-{}.json'.format(delta)
    if not op.isfile(fname):
        return

    with open(fname) as fd:
        dct = jsonio.decode(json.load(fd))

    if 'P_asvv' not in dct:
        from c2db.borncharges import borncharges
        borncharges(delta)
        with open(fname) as fd:
            dct = jsonio.decode(json.load(fd))

    P_davv.append(dct['P_asvv'][:, 0])
    P_davv.append(dct['P_asvv'][:, 1])
    data['Z_avv'] = -dct['Z_avv']

    P_davv = np.array(P_davv)
    data['borndata'] = [[-0.01, 0.01], P_davv]


def print_results(filename='data-borncharges/borncharges-0.01.json'):
    np.set_printoptions(formatter={'float': '{: 0.3f}'.format})
    import os.path as op
    if not op.isfile(filename):
        return

    with open(filename) as fd:
        dct = jsonio.decode(json.load(fd))
    title = """
    BORNCHARGES
    ===========
    """
    print(title)
    print(-dct['Z_avv'])


def main(args):
    borncharges(**args)


short_description = 'Calculate Born charges'
dependencies = ['gs.py']
parser = argparse.ArgumentParser(description=short_description)
help = 'Atomic displacement when moving atoms in Å'
parser.add_argument('-d', '--displacement', help=help, default=0.01,
                    type=float)
help = 'Set kpoint density for calculation of berryphases'
parser.add_argument('-k', '--kpointdensity', help=help, default=6.0,
                    type=float)
help = 'Folder where data is put'
parser.add_argument('-f', '--folder', help=help,
                    default='data-borncharges')


if __name__ == '__main__':
    args = vars(parser.parse_args())
    main(args)
